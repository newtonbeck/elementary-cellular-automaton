import React, { Component } from 'react';
import { View, Text, TextInput, StyleSheet, Platform, Alert, Dimensions } from 'react-native';
import { Button } from '../components/form';
import { createAutomaton } from '../automaton/automatonFactory';
import { InitialGeneration } from '../components/initialGeneration';
import { createInitialCells } from '../initialCellsFactory';

export default class SetupScreen extends Component {

  static navigationOptions = {
    title: 'Setup',
  };

  constructor() {
    super();
    let { width } = Dimensions.get('window');
    this.state = {
      cells: createInitialCells(width),
      rule: '',
    }
    this._goToAutomaton = this._goToAutomaton.bind(this);
    this._onPressCell = this._onPressCell.bind(this);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Touch the cells bellow to change initial generation:</Text>
        <InitialGeneration cells={this.state.cells} onPressCell={this._onPressCell} />
        <View style={styles.formContainer}>
          <TextInput style={styles.ruleInput}
            placeholder='Rule from 0 to 255'
            keyboardType='numeric'
            value={this.state.rule}
            onChangeText={(text) => this.setState({ rule: text })} />
          <Button title='Go to automaton' onPress={() => this._goToAutomaton()} />
        </View>
      </View>
    );
  }

  _goToAutomaton() {
    let { navigate } = this.props.navigation;
    let { cells, rule } = this.state;
    let automaton = createAutomaton(cells, parseInt(rule));
    if (automaton === null || rule === '') {
      Alert.alert('Rule must be a number between 0 and 255 inclusive.');
      return;
    }
    navigate('Automaton', {
      automaton
    })
  }

  _onPressCell(cell, index) {
    let clonedCells = this.state.cells.slice(0);
    if (cell === 1) {
      clonedCells[index] = 0;
    } else {
      clonedCells[index] = 1;
    }
    this.setState({cells: clonedCells});
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  formContainer: {
    padding: 10,
  },
  ruleInput: {
    padding: 10,
    borderBottomWidth: (Platform.OS === 'ios' ? 1 : 0),
    borderBottomColor: 'grey',
    marginBottom: 10,
  },
});
