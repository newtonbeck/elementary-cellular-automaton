import React, { Component } from 'react';
import { View, TextInput, Text, TouchableHighlight, FlatList, StyleSheet } from 'react-native';
import { Button } from '../components/form';
import { Automaton } from '../components/automaton';

export default class AutomatonScreen extends Component {

  static navigationOptions = {
    title: 'Automaton',
  };

  constructor(props) {
    super(props);
    let { automaton } = this.props.navigation.state.params;
    this.state = {
      timer: null,
      automaton,
    }
    this._start = this._start.bind(this);
    this._stop = this._stop.bind(this);
    this._nextStep = this._nextStep.bind(this);
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.controlsContainer}>
          <Button onPress={this._nextStep} title='Next step' />
          <Button onPress={this.state.timer ? this._stop : this._start} title={this.state.timer ? 'Pause' : 'Play'} />
        </View>
        <View style={styles.automatonContainer}>
          <Automaton automaton={this.state.automaton} />
        </View>
      </View>
    );
  }

  _start() {
      let timer = setInterval(() => {
        this._nextStep();
      }, 500);
      this.setState({
        timer
      });
  }

  _stop() {
    clearInterval(this.state.timer);
    this.setState({
      timer: null,
    });
  }

  _nextStep() {
    let automaton = this.state.automaton.evolve();
    this.setState({
      automaton,
    });
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  automatonContainer: {
    flex: 1,
  },
  controlsContainer: {
    height: 75,
    backgroundColor: 'lightgrey',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
  },
});
