import Neighborhood from './Neighborhood';

export default class Generation {

  constructor(cells) {
    this.cells = cells;
  }

  _getNeighborhoodAt(index) {
    let left =  index > 0 ? index - 1 : this.cells.length - 1;
    let right = (index + 1) % this.cells.length;

    return new Neighborhood(this.cells[left], this.cells[index], this.cells[right]);
  }

  getAllNeighborhoods() {
    return this.cells.map((_, index) => this._getNeighborhoodAt(index));
  }

  getCells() {
    return this.cells;
  }

}
