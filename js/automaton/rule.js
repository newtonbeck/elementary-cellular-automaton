import { toBinary, to8Bits, toDecimal } from '../binary';

export const createRule = (number) => {
  if (number < 0 || number > 255) {
    return null;
  }
  return (neighborhood) => {
    let binary = to8Bits(toBinary(number));
    let rule = Array
      .from(binary)
      .reverse()
      .map((digit) => parseInt(digit));
    return applyRule(rule, neighborhood)
  };
}

const applyRule = (rule, neighborhood) => {
  let index = toDecimal(neighborhood.toBinary());
  return rule[index];
}
