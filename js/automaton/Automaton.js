import Generation from './Generation';

export default class Automaton {

  constructor(generations, rule) {
    this.generations = generations;
    this.rule = rule;
  }

  evolve() {
    let lastGenerationIndex = this.generations.length - 1;
    let lastGeneration = this.generations[lastGenerationIndex];
    let nextGenerationCells = lastGeneration
      .getAllNeighborhoods()
      .map((neighborhood) => this.rule(neighborhood));
    let nextGeneration = new Generation(nextGenerationCells);
    let newGenerations = this.generations.concat(nextGeneration);
    return new Automaton(newGenerations, this.rule);
  }

  getGenerations() {
    return this.generations;
  }

}
