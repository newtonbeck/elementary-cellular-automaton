import Automaton from './Automaton';
import Generation from './Generation';
import { createRule } from './rule';

export const createAutomaton = (firstGenerationCells, ruleNumber) => {
  let rule = createRule(ruleNumber);
  if (rule === null) {
    return null;
  }
  let firstGeneration = new Generation(firstGenerationCells);
  return new Automaton([firstGeneration], rule);
};
