export default class Neighborhood {

  constructor(left, center, right) {
    this.left = left;
    this.center = center;
    this.right = right;
  }

  toBinary() {
    return `${this.left}${this.center}${this.right}`;
  }

}
