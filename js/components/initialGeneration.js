import React from 'react';
import { Text, View, StyleSheet } from 'react-native'

export const InitialGeneration = ({ cells, onPressCell }) => {
  return (
    <View style={styles.cellsContainer}>
    {
      cells.map((cell, index) => (<TouchableCell key={`TouchableCell-${index}`} cell={cell} index={index} onPressCell={onPressCell} />))
    }
    </View>
  );
};

const TouchableCell = ({ cell, index, onPressCell }) => {
  return (
    <Text
      style={cell === 1 ? styles.cellOne : styles.cellZero}
      key={`TouchableCell-${index}`}
      onPress={() => onPressCell(cell, index)}>
      {cell}
    </Text>
  );
};

const styles = StyleSheet.create({
  cellsContainer: {
    height: 15,
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  cellOne: {
    flex: 1,
    backgroundColor: 'black',
    color: 'black',
    margin: 1,
  },
  cellZero: {
    flex: 1,
    backgroundColor: 'lightgrey',
    color: 'lightgrey',
    margin: 1,
  }
});
