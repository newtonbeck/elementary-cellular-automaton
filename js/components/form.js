import React from 'react';
import { TouchableHighlight, Text, StyleSheet } from 'react-native';
import { createAutomaton } from '../automaton/automatonFactory';

export const Button = ({ onPress, title }) => {
  return (
    <TouchableHighlight
      style={styles.button}
      onPress={onPress}>
      <Text>{title}</Text>
    </TouchableHighlight>
  );
}

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'aliceblue',
    padding: 20,
  },
});
