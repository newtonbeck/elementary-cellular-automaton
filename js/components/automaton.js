import React from 'react';
import { FlatList, View, StyleSheet } from 'react-native'

export const Automaton = ({ automaton }) => {
  return (
    <FlatList
      keyExtractor={(item, index) => `FlatList-${index}` }
      data={automaton.getGenerations()}
      renderItem={({item, index}) => { return (<Generation key={`Generation-${index}`} number={index} generation={item} />) }} />
  );
};

const Generation = ({ number, generation }) => {
  return (
    <View key={`View-${number}`} style={styles.generation}>
      {
        generation.getCells().map((cell, index) => { return (<Cell key={`Cell-${number}${index}`} generationNumber={number} cellNumber={index} value={cell} />) })
      }
    </View>
  );
};

const Cell = ({ generationNumber, cellNumber, value }) => {
  if (value === 1) {
    return (<View key={`View-${generationNumber}${cellNumber}`} style={styles.cellOne} />);
  } else {
    return (<View key={`View-${generationNumber}${cellNumber}`} style={styles.cellZero} />);
  }
};

const styles = StyleSheet.create({
  generation: {
    height: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  cellOne: {
    flex: 1,
    backgroundColor: 'black',
    margin: 1,
  },
  cellZero: {
    flex: 1,
    backgroundColor: 'lightgrey',
    margin: 1,
  }
});
