import AutomatonScreen from './screens/AutomatonScreen';
import SetupScreen from './screens/SetupScreen';
import { StackNavigator } from 'react-navigation';

const App = StackNavigator({
  Setup: {
    screen: SetupScreen,
  },
  Automaton: {
    screen: AutomatonScreen,
  }
});

export default App;
