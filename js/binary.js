export const toBinary = (decimal) => {
  return _toBinary(decimal, '');
};

const _toBinary = (decimal, binary) => {
  if (decimal < 2) {
    return decimal.toString() + binary;
  }
  let quotient = Math.floor(decimal / 2);
  let remainder = decimal % 2;
  return _toBinary(quotient, remainder.toString() + binary);
}

export const toDecimal = (binary) => {
  return Array
    .from(binary)
    .reverse()
    .map((digit, index) => {
      return digit * Math.pow(2, index);
    })
    .reduce((oneDigit, anotherDigit) => {
      return oneDigit + anotherDigit;
    });
}

export const to8Bits = (binary) => {
  if (binary.length >= 8) {
    return binary;
  }
  return to8Bits('0' + binary);
}
