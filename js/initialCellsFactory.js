export const createInitialCells = (width) => {
  let numberOfCells = Math.floor(width / 15);
  return Array.from('0'.repeat(numberOfCells))
    .map((cell) => parseInt(cell));
};
