# Elementary Cellular Automaton

Este é o projeto de um autômato celular elementar feito em React Native.

## Executando no Android
Para executar este projeto no Android basta abrir o projeto no Android Studio e clicar em Run. Não se esqueça de executar o comando `npm start` para subir o servidor do Metro Bundler.

## Executando no iOS
Para executar este projeto no iOS basta abrir o projeto no XCode e clicar em Run. Não se esqueça de executar o comando `npm start` para subir o servidor do Metro Bundler.

## Explicações sobre a estrutura do projeto
Todo o código JS está dentro do diretório `js`. Dentro deste diretório temos os seguintes diretórios:
- `automaton`: diretório onde está todo o modelo do autômato Cellular
- `components`: componentes de apresentação do projeto, são todos imutáveis e criados como lambda components
- `screens`: são as telas do projeto, atuam como container components orquestrando os componentes de apresentação e suas interações na tela

Os testes de unidade estão dentro do diretório `__tests__`.

## Explicações sobre decisões de código do projeto
- Criei as classes `Automaton`, `Generation` e `Neighborhood` pois fez sentido para mim manter os dados e os comportamentos dessas classes próximos um do outro.
- A `rule` não é uma classe, mas uma função, pois ela representa um processo, portanto uma função me pareceu muito mais justa do que criar uma classe ou algo do gênero.
- As funções de conversão de bases (binário - decimal) foram construídas usando recursão de cauda. Após uma rápida pesquisa descobri que o ES6 tem otimizações para recursão de causa, o que me encorajou a tomar esse caminho na implementação dos conversores.
- Como o projeto é composto de somente 2 telas acabei não usando Redux para compartilhar estado entre elas, que no caso poderia ser o autômato. Até por que toda a evolução do autômato só interessa à segunda tela.
- Criei testes de unidade para o modelo porém não criei testes de interface para as telas. Não que eu não os ache importante, mas eu ainda não sei fazê-los no mobile.

## Explicações sobre a interface do projeto
- Ficou feia
- Não testei o projeto num device real, somente nos emuladores. Provavelmente as áreas de toque das células estão muito pequenas, o que precisaria ser revisto.
