import Neighborhood from '../../js/automaton/Neighborhood';

test('should return binary 000 to 000 neighborhood', () => {
  let neighborhood = new Neighborhood(0, 0, 0);
  expect(neighborhood.toBinary()).toBe('000');
});

test('should return binary 001 to 001 neighborhood', () => {
  let neighborhood = new Neighborhood(0, 0, 1);
  expect(neighborhood.toBinary()).toBe('001');
});

test('should return binary 010 to 010 neighborhood', () => {
  let neighborhood = new Neighborhood(0, 1, 0);
  expect(neighborhood.toBinary()).toBe('010');
});

test('should return binary 011 to 011 neighborhood', () => {
  let neighborhood = new Neighborhood(0, 1, 1);
  expect(neighborhood.toBinary()).toBe('011');
});

test('should return binary 100 to 100 neighborhood', () => {
  let neighborhood = new Neighborhood(1, 0, 0);
  expect(neighborhood.toBinary()).toBe('100');
});

test('should return binary 101 to 101 neighborhood', () => {
  let neighborhood = new Neighborhood(1, 0, 1);
  expect(neighborhood.toBinary()).toBe('101');
});

test('should return binary 110 to 110 neighborhood', () => {
  let neighborhood = new Neighborhood(1, 1, 0);
  expect(neighborhood.toBinary()).toBe('110');
});

test('should return binary 111 to 111 neighborhood', () => {
  let neighborhood = new Neighborhood(1, 1, 1);
  expect(neighborhood.toBinary()).toBe('111');
});
