import { createAutomaton } from '../../js/automaton/automatonFactory';

test('should create an automaton with an initial generation of 10 cells', () => {
  let firstGenerationCells = [0, 0, 0, 0, 0, 1, 0, 0, 0, 0];
  let rule = 110;
  let automaton = createAutomaton(firstGenerationCells, rule);
  let lastGeneration = automaton.getGenerations()[0];
  expect(lastGeneration.getCells()).toEqual([0, 0, 0, 0, 0, 1, 0, 0, 0, 0])
});

test('should not create an automaton with an invalid rule', () => {
  let firstGenerationCells = [0, 0, 0, 0, 0, 1, 0, 0, 0, 0];
  let rule = -1;
  let automaton = createAutomaton(firstGenerationCells, rule);
  expect(automaton).toBe(null);
});
