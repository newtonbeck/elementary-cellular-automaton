import Generation from '../../js/automaton/Generation';

test('should get neighborhood of first element', () => {
  let generation = new Generation([1, 0, 1]);
  expect(generation._getNeighborhoodAt(0).toBinary()).toBe('110');
});

test('should get neighborhood of central element', () => {
  let generation = new Generation([1, 0, 1]);
  expect(generation._getNeighborhoodAt(1).toBinary()).toBe('101');
});

test('should get neighborhood of last element', () => {
  let generation = new Generation([1, 0, 1]);
  expect(generation._getNeighborhoodAt(2).toBinary()).toBe('011');
});

test('should get all neighborhoods', () => {
  let generation = new Generation([1, 0, 1]);
  let neighborhoods = generation.getAllNeighborhoods();
  let binaryNeighborhood = neighborhoods.map(neighborhood => neighborhood.toBinary());
  expect(binaryNeighborhood).toEqual(['110', '101', '011']);
});
