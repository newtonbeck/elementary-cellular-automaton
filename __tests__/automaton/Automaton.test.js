import Generation from '../../js/automaton/Generation';
import Automaton from '../../js/automaton/Automaton';
import { createRule } from '../../js/automaton/rule';

test('cellular automaton should evolve lastGeneration using rule 110', () => {
  let rule = createRule(110);
  let firstGeneration = new Generation([0, 1, 0, 1, 0]);
  let cellularAutomaton = new Automaton([firstGeneration], rule);
  let automaton = cellularAutomaton.evolve();
  let evolvedCells = automaton
    .getGenerations()
    .map(generation => generation.getCells());
  expect(evolvedCells).toEqual([[0, 1, 0, 1, 0], [1, 1, 1, 1, 0]]);
});
