import Neighborhood from '../../js/automaton/Neighborhood';
import { createRule } from '../../js/automaton/rule';

test('should apply rule 110 to neighborhood 000', () => {
  let neighborhood = new Neighborhood(0, 0, 0);
  let applyRule = createRule(110);
  expect(applyRule(neighborhood)).toBe(0);
});

test('should apply rule 110 to neighborhood 001', () => {
  let neighborhood = new Neighborhood(0, 0, 1);
  let applyRule = createRule(110);
  expect(applyRule(neighborhood)).toBe(1);
});

test('should apply rule 110 to neighborhood 010', () => {
  let neighborhood = new Neighborhood(0, 1, 0);
  let applyRule = createRule(110);
  expect(applyRule(neighborhood)).toBe(1);
});

test('should apply rule 110 to neighborhood 011', () => {
  let neighborhood = new Neighborhood(0, 1, 1);
  let applyRule = createRule(110);
  expect(applyRule(neighborhood)).toBe(1);
});

test('should apply rule 110 to neighborhood 100', () => {
  let neighborhood = new Neighborhood(1, 0, 0);
  let applyRule = createRule(110);
  expect(applyRule(neighborhood)).toBe(0);
});

test('should apply rule 110 to neighborhood 101', () => {
  let neighborhood = new Neighborhood(1, 0, 1);
  let applyRule = createRule(110);
  expect(applyRule(neighborhood)).toBe(1);
});

test('should apply rule 110 to neighborhood 110', () => {
  let neighborhood = new Neighborhood(1, 1, 0);
  let applyRule = createRule(110);
  expect(applyRule(neighborhood)).toBe(1);
});

test('should apply rule 110 to neighborhood 111', () => {
  let neighborhood = new Neighborhood(1, 1, 1);
  let applyRule = createRule(110);
  expect(applyRule(neighborhood)).toBe(0);
});

test('should not create rule when number is lesser than 0', () => {
  let applyRule = createRule(-1);
  expect(applyRule).toBe(null);
});

test('should not create rule when number is greater than 255', () => {
  let applyRule = createRule(256);
  expect(applyRule).toBe(null);
});
