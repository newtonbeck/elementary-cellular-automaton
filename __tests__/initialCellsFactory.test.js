import { createInitialCells } from '../js/initialCellsFactory';

test('should create 10 cells for screen of 150 width', () => {
  let cells = createInitialCells(150);
  expect(cells).toEqual([0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
});
