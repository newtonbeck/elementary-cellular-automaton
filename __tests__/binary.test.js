import { toBinary, toDecimal, to8Bits } from '../js/binary';

test('should convert even decimal number to binary number', () => {
  expect(toBinary(78)).toBe('1001110');
});

test('should convert odd decimal number to binary number', () => {
  expect(toBinary(67)).toBe('1000011');
});

test('should convert even binary number to decimal number', () => {
  expect(toDecimal('1001110')).toBe(78);
});

test('should convert odd binary number to decimal number', () => {
  expect(toDecimal('1000011')).toBe(67);
});

test('should format 1001110 to 01001110', () => {
  expect(to8Bits('1001110')).toBe('01001110');
});

test('should not format 10100100000011001001', () => {
  expect(to8Bits('10100100000011001001')).toBe('10100100000011001001');
});
